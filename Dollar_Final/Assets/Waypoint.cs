﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {
    // Use this for initialization
    void Awake() {
        Transform waypointsTransform = GameObject.FindWithTag("Waypoints").transform;
        transform.SetParent(waypointsTransform, false);

    }
}

