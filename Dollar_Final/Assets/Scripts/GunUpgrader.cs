﻿using CompleteProject;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunUpgrader : MonoBehaviour
{
    public int requiredPoints;
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (WaveSpawner.points >= requiredPoints)
            {
                Destroy(gameObject);
                //shootingScript.damagePerShot *= 1.5;
                //Done_PlayerShooting shootingScript = GetComponent<Done_PlayerShooting>();
                //Done_PlayerShooting shootingScript toInt();
                //shootingScript.damagePerShot *= 1.5;
                //GetComponent<Done_PlayerShooting>().damagePerShot *= 1.5;
                //GetComponent<Done_PlayerShooting>().timeBetweenBullets *= 1.5;
                Done_PlayerShooting.damagePerShot += 5;

                WaveSpawner.points -= requiredPoints;
            }
        }
    }
}
