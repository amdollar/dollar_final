﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour {
    public BaseHealth baseHealth;
    public GameObject baseobject;
    public WaveSpawner wavespawner;

    Animator anim;
    float restartTimer;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (baseHealth.baseHealth <= 0)
        {
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime;
            if (restartTimer >= 5)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (wavespawner.waveIndex >= 20)
            {
                anim.SetTrigger("Win");
                restartTimer += Time.deltaTime;
                if (restartTimer >= 5)
                {
                    Application.LoadLevel(Application.loadedLevel);
                }
            }
        }
    }
}
