﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public Transform enemyPrefab;
    public Transform followingenemyPrefab;
    public Transform spawnPoint;
    public Transform playerportal2;
    public Transform playerportal3;
    public Transform portallight;
    public bool portalstatus = false;

    public float timeBetweenWaves = 10f;
    private float countdown = 5f;

    public Image startFader;
    public Text startText;
    public Text portalText;
    public Text waveCountdownText;
    public Text waveNumText;
    public float followenemyTimer = 10f;
    public int waveIndex = 1;
    AudioSource audio1;
    AudioSource audio2;
    public static int points = 0;
    public Text pointsUI;
    void Awake()
    {
        var aSources = GetComponents<AudioSource>();
        audio1 = aSources[0];
        audio2 = aSources[1];
    }
    private void Update()
    {   if (countdown <= 0f)
        {
            startFader.enabled = false;
            startText.enabled = false;
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves;
        }
        countdown -= Time.deltaTime;
        followenemyTimer += Time.deltaTime;
        if(followenemyTimer>=7)
        {
            SpawnEnemy(followingenemyPrefab);
            followenemyTimer = 0;
        }
        pointsUI.text = points.ToString();
        waveCountdownText.text = Mathf.Round(countdown).ToString();
        waveNumText.text = waveIndex.ToString();
        if (waveIndex > 20 && portalstatus == false)
        {
            portalText.enabled = true;
            audio2.Play();
            playerportal2.transform.SetPositionAndRotation(new Vector3(-30.37f, 1.9f, -0.14f), playerportal2.rotation);
            playerportal3.transform.SetPositionAndRotation(new Vector3(-32.24f, 1.9f, -0.19f), playerportal3.rotation);
            Instantiate(portallight, playerportal3.position, playerportal3.rotation);
            //ensures only one light is spawned
            portalstatus = true;
        }
    }
    IEnumerator SpawnWave()
    {
        SpawnEnemy(enemyPrefab);
        yield return new WaitForSeconds(0.5f);
        waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy(enemyPrefab);
            yield return new WaitForSeconds(0.5f);
            
            //If wave is greater than 10 make it so there is a little break between the wave to make it easier
            if (i <= 5)
            {
                timeBetweenWaves = 7f;

            }
            else if (i < 15 && i > 5)
            {
                timeBetweenWaves = 10f;

            }
            else if (i > 15)
            {
                timeBetweenWaves = 15f;
                SpawnEnemy(enemyPrefab);
                yield return new WaitForSeconds(0.5f);

            }
            //If wave is greater than 10 make it so there is a little break between the wave to make it easier

        }
    }
    void SpawnEnemy(Transform enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
    }
}
