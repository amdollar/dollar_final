﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBuyer : MonoBehaviour
{
    public Transform wall;
    public Transform wallBuyer;
    public int requiredPoints;
    public Transform waypoint2;
    public Vector3 wapointposition;


    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (WaveSpawner.points >= requiredPoints)
            {
                Destroy(gameObject);
                Instantiate(wall, wallBuyer.position, wallBuyer.rotation);
                WaveSpawner.points -= requiredPoints;
                waypoint2.transform.SetPositionAndRotation(wapointposition, wallBuyer.rotation);
            }
        }
    }
}

