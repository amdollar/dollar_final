﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float startingHealth = 100f;
    public float currentHealth;
    CapsuleCollider capsuleCollider;

    Transform target;
    public Image healthBar;



    // Use this for initialization
    void Awake()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
        currentHealth = startingHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0f)
        {
            Destroy(gameObject);
            WaveSpawner.points += 5;
        }
        healthBar.fillAmount = currentHealth / startingHealth;
        
    }


    public void TakeDamage(float amount)
    {
        currentHealth -= amount;
        print("hit");

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            TakeDamage(50f);
            Destroy(other.gameObject);
        }
    }
}
