﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBuyer : MonoBehaviour
{
    public Transform turret;
    public Transform turretBuyer;
    public int requiredPoints;



    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (WaveSpawner.points >= requiredPoints)
            {
                Destroy(gameObject);
                Instantiate(turret, turretBuyer.position, turretBuyer.rotation);
                WaveSpawner.points -= requiredPoints;
            }
        }
    }
}
