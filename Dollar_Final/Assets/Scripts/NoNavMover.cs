﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoNavMover : MonoBehaviour
{
    Transform player;
    //PlayerHealth playerHealth;
    //EnemyHealth enemyHealth;
    //UnityEngine.AI.NavMeshAgent nav;
    Rigidbody rb;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //playerHealth = player.GetComponent <PlayerHealth> ();
        //enemyHealth = GetComponent <EnemyHealth> ();
        // nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        //if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        //{
        //nav.SetDestination(player.position);

        //}
        //else
        //{
        //    nav.enabled = false;
        //}


        Vector3 direction = player.position - transform.position;
        direction = direction.normalized;
        rb.velocity += direction * Time.deltaTime * 300;
    }
}
