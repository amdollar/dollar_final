﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseHealth : MonoBehaviour {
    public float baseHealth= 100f;
    public Text healthText;



    // Use this for initialization
    void awake () {

    }
	
	// Update is called once per frame
	void Update () {
        healthText.text = baseHealth.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Shootable")
        {
            baseHealth -= 5;
        }
   
    }
}
