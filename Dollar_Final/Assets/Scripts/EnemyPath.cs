﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPath : MonoBehaviour {

    //public Transform[] waypoints;

    public float speed = 0.05f;
    private Transform target;
    private int wavepointIndex = 0;
    GameObject enemy;

    private void Start()
    {

            target = Waypoints.points[0];
    }
    private void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
        transform.rotation= Quaternion.LookRotation(dir, Vector3.up);

        if (Vector3.Distance(transform.position, target.position) <= 1f)
        {
            GetNextWaypoint();
        }
    }
    void GetNextWaypoint()
    {
        //if(wavepointIndex >= Waypoints.points.Length - 1)
        //{
         //   Destroy(gameObject);
         //   return;
       // }
        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Base")
        {
            Destroy(gameObject);
        }
    }

}
